import unittest
from histoire_automobile import (
    VerificationFichier,
    ChangementDelimiteur,
    ChangementColonnes
)

dictionaire = {
    'voiture' : {'adress':'1 |','carrosserie':'2 |','categorie':'3 |','couleur':'4 |','cylindre':'5 |',},
}

dictionaireSeparateur = {
    'voiture' : {'adress':'1 ,','carrosserie':'2 ,','categorie':'3 ,','couleur':'4 ,','cylindre':'5 ,',},
}

dictionaireColonne = {
    'voiture' : {'adress titulaire':'1 ,','nom':'2 ,','prenom':'3 ,','imatriculation':'4 ,','date_imatriculation':'5 ,',},
}

class TestHistoire_AutomobileFunctions(unittest.Testcase):
    
    def test_VerificationFichier(VerificationFichier):
        self.assertEqual(histoire_automobile.VerificationFichier('eggs.csv'),true)
        self.assertEqual(histoire_automobile.VerificationFichier('eggs.txt'),false)

    def test_ChangementDelimiteur(ChangementDelimiteur):
        self.assertEqual(histoire_automobile.ChangementDelimiteur(dictionaire),dictionaireSeparateur)

    def test_ChangementColonnes(ChangementColonnes):
        self.assertEqual(histoire_automobile.ChangementColonnes(dictionaire),dictionaireColonne)