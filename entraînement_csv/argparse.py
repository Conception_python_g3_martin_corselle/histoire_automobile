import argparse
parser = argparse.ArgumentParser(description="This is a usefull description.")
parser.add_argument("echo", help="echo the string you use here")
parser.add_argument("square", help="display a square of a given number", type=int)
parser.add_argument("--verbosity", help="increase output verbosity") #optional args
args = parser.parse_args()
print(args.echo)
print(args.square)