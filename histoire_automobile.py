import os
import os.path
import csv
import sys

def Verification(fichier):	
    csv = False
    extension=os.path.splitext(fichier)
    if ".csv" in extension[1] :
        csv= True

    else :
        csv = False
    return csv

def ChangementDelimiteur(fichier, delimiteur):
    fichierAModifier=open(fichier,'r')
    texte=fichierAModifier.read().replace('|',delimiteur)
    fichierAModifier.close()
    fichierAModifier=open(fichier,'w')
    fichierAModifier.write(texte)
    fichierAModifier.close()

def ChangementFormat(fichier):
    fichierAConvertir=open(fichier,'r')
    nom=fichierAConvertir.name
    nom=nom.replace('.txt','.csv')
    fichierAConvertir.close()
    os.rename(fichier,nom)

def copie_dico(nom_fichier):
    with open(nom_fichier) as csvfile :
        reader = csv.reader(csvfile, delimiter = '|')
    indice = 0
    for row in reader:
        if(indice % 2 == 0):
            nom_col = row
        else :
            histoire_automobile[str(nom_col)] =  row
        indice += 1
    return histoire_automobile

def transform_row(row, config):
    """
    Translate given row to a new dict from configuration

    Parameters
    ----------
    row : dict
        The dict source
    config : dict
        Corresponding key to attribute from old dict
    Returns
    -------
    dict
        the row transformed inside a new dict
    """
    new_row = {}
    for key, attr in config.items():
        new_row[key] = row[attr]
    return new_row


def transform_rows(rows, config):
    """
    Translate given rows to a lsit of dict from configuration

    Parameters
    ----------
    rows : list
        List of rows
    config : dict
        Corresponding key to attribute from old dict
    Returns
    -------
    list
        The rows translated and transformed to new dict
    """
    return [transform_row(row, config) for row in rows]

def Changement_colonnes(histoire_automobile):
    indice = 0
    for attr in histoire_automobile.items():
        indice = indice+1

        if(indice == 1) :
            histoire_automobile["type"] = histoire_automobile["type_variante_version".attr]
                    
        if(indice == 2) :
            histoire_automobile["variante"] = histoire_automobile["type_variante_version".attr]
                    
        if(indice == 3) :
            histoire_automobile["version"] = histoire_automobile["type_variante_version".attr]
    
    dict_config = {"adresse_titulaire" : "address", "nom" : "name", "prenom" : "firstname", "immatriculation" : "immat", "date_immatriculation" : "date_immat", "vin" : "vin", "marque" : "marque", "couleur" : "couleur", "carrosserie" : "carrosserie", "categorie" : "categorie", "cylindree" : "cylindree", "energie" : "energy", "places" : "places", "poids" : "poids", "puissance" : "puissance" , "type" : "type", "variante" : "variante", "version" : "version", "denomination_commerciale" : "denomination"}
    rows = list(histoire_automobile.keys())
    histoire_automobile = transform_rows(rows, dict_config)
    return histoire_automobile

def copie_fichier(fichier, rows, fieldnames, delimiteur):
    with open(filepath, 'w') as csvout:
        csvwriter = csv.DictWriter(
            csvout, fieldnames=fieldnames, delimiter=delimiteur)
        csvwriter.writeheader()
        csvwriter.writerows(rows)


if __name__ == "__main__":
    fichier = sys.argv[1]
    delimiteur = sys.argv[2]
    verif_fic = Verification(fichier)
    if(verif_fic == True) :
        ChangementDelimiteur(fichier, delimiteur)
        ChangementFormat(fichier)
        histoire_automobile = copie_dico(fichier)
        rows = list(histoire_automobile.keys())
        fieldnames = list(histoire_automobile)
        histoire_automobile = Changement_colonnes(histoire_automobile)
        fichier = copie_fichier(fichier, rows, fieldnames, delimiteur)


